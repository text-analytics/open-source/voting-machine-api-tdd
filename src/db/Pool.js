import { Pool } from 'pg'

export const config = {
    application_name: 'voting-machine-api',
    host: process.env.POSTGRES_HOST,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB
}

/** @type {Pool} Singleton Pool Instance */
export default new Pool(config)