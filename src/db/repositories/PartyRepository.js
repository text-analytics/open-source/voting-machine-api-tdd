import Pool from '../Pool'
import Repository from './Repository'

class PartyRepository extends Repository {

    constructor() {
        super('parties')
    }

    async list() {
        const sql = `SELECT * FROM ${this.tableName}`
        const { rows } = await Pool.query(sql)
        return rows
    }

    /**
     * @param {string} name Party Name
     */
    async add(name) {
        const sql = `INSERT INTO ${this.tableName}(name) VALUES ($1) RETURNING *`
        const { rows: [newParty] } = await Pool.query(sql, [name])
        return newParty
    }
}

/** @type {PartyRepository} Singleton PartyRepository Instance */
export default new PartyRepository()