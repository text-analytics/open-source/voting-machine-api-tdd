/**
 * @param {import('koa').Context} ctx
 * @param {Promise} next
 */
async function errorHandler(ctx, next) {
    try {
        await next()
    } catch (err) {
        if (process.env.NODE_ENV !== 'test') console.error(err)
        const { status = 500, message = 'Internal Server Error'} = err
        ctx.status = status
        ctx.body = { status, message }
    }
}

export default () => {
    return errorHandler
}