import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'
import chaiHttp from 'chai-http'

import { startServer } from '../src/server'

chai.use(chaiHttp)
chai.use(chaiAsPromised)

// starts on unused port
const server = startServer()

export default chai.request.agent(server)