/**
 * @param {import('node-pg-migrate').MigrationBuilder} pgm 
 */
export function up(pgm) {
    pgm.addExtension('uuid-ossp', { ifNotExists: true })
}

export const down = false