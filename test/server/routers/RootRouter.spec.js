import { expect } from 'chai'

import appAgent from '../../app-agent'

describe('Ping Endpoint', async () => {
    it('should return pong', async () => {
        const response = await appAgent.get('/ping')
        expect(response).to.be.json
        expect(response).to.have.status(200)
        expect(response.body).to.equal('pong')
    })

    it('should 404 when path not found', async () => {
        const response = await appAgent.get('/not-found')
        expect(response).to.be.json
        expect(response).to.have.status(404)
    })
})