import Router from 'koa-router'
import { BadRequest } from 'http-errors'

import Candidate from '../../../db/repositories/CandidateRepository'

export default class RootRouter extends Router {

    constructor() {
        super({ prefix: '/candidates'})

        this.get('/', this.list.bind(this))
        this.post('/', this.add.bind(this))
    }

    /**
     * @swagger
     * definitions:
     * 
     *   NewCandidate:
     *     type: object
     *     required:
     *       - name
     *       - party_uuid
     *     properties:
     *       name:
     *         type: string
     *       party_uuid:
     *         type: string
     *         format: uuid
     *         example: 00000000-0000-0000-0000-000000000000
     *   
     *   Candidate:
     *     allOf:
     *       - $ref: '#/definitions/NewCandidate'
     *       - type: object
     *       - required:
     *         - uuid
     *         properties:
     *           uuid:
     *             type: string
     *             format: uuid
     *             example: 00000000-0000-0000-0000-000000000000
     */

    /**
     * @swagger
     * /candidates:
     *   get:
     *     tags:
     *       - Candidates
     *     summary: List Candidates
     *     produces:
     *      - application/json
     *     responses:
     *       200:
     *         description: Candidate List
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/Candidate'
     * @private
     * @param {import('koa-router').RouterContext} ctx context
     */
    async list(ctx) {
        ctx.body = await Candidate.list()
    }

    /**
     * @swagger
     * /candidates:
     *   post:
     *     tags:
     *       - Candidates
     *     summary: Create Candidate
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: candidate
     *         description: new candidate object
     *         in:  body
     *         required: true
     *         type: string
     *         schema:
     *           $ref: '#/definitions/NewCandidate'
     *     responses:
     *       201:
     *         description: candidate
     *         schema:
     *           $ref: '#/definitions/Candidate'
     * @private
     * @param {import('koa-router').RouterContext} ctx context
     */
    async add(ctx) {
        const { name, party_uuid: partyId } = ctx.request.body
        if (!name) throw new BadRequest("missing required body parameter 'name'")
        if (!partyId) throw new BadRequest("missing required body parameter 'party_uuid'")

        const candidate = await Candidate.add(name, partyId)

        ctx.status = 201
        ctx.set('Location', `/v0/candidates/${candidate.uuid}`)
        ctx.body = candidate
    }
}