import Router from 'koa-router'
import swaggerJsDoc from 'swagger-jsdoc'
import koaSwagger from 'koa2-swagger-ui'

import CandidateRouter from './v0/CandidateRouter'
import PartyRouter from './v0/PartyRouter'

export default class V0Router extends Router {

    constructor() {
        super({ prefix: '/v0' })

        const swaggerSpec = swaggerJsDoc({
            swaggerDefinition: {
                info: {
                    title: 'Voting Machine',
                    description: `your vote counts to me`
                },
                basePath: '/v0'
            },
            apis: [`./src/server/routers/v0/**/*.js`],
        })

        this.get('/swagger',
            koaSwagger({
                hideTopbar: true,
                routePrefix: false,
                swaggerOptions: {
                    spec: swaggerSpec,
                    docExpansion: 'none'
                },
            }),
        )

        this.use(new CandidateRouter().routes())
        this.use(new PartyRouter().routes())
    }
}
