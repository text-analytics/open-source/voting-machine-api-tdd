import sinon from 'sinon'
import { expect } from 'chai'

import appAgent from '../../../app-agent'

import Candidate from '../../../../src/db/repositories/CandidateRepository'

describe('Candidate Router', async () => {

    let mockCandidate
    
    beforeEach(async () => {
        mockCandidate = sinon.mock(Candidate)
    })

    afterEach(async () => {
        mockCandidate.restore()
    })


    it('should list candidates', async () => {
        const candidates = [{uuid: 'MONKEY_UUID', name: 'MONKEY', party_uuid: 'PARTY_1_UUID'}, {uuid: 'PLATYPUS_UUID', name: 'PLATYPUS', party_uuid: 'PARTY_2_UUID'}]

        mockCandidate.expects('list').once().resolves(candidates)

        const result = await appAgent.get('/v0/candidates')

        expect(result).to.be.json
        expect(result).to.have.status(200)
        expect(result.body).to.deep.equal(candidates)
        mockCandidate.verify()
    })

    it('should add a new candidate', async () => {
        const name = 'Platypus'
        const partyId = 'PARTY_UUID'
        const payload = { name, party_uuid: partyId }
        const candidate = {uuid: 'PLATYPUS_UUID', name, party_uuid: partyId}

        mockCandidate.expects('add').withExactArgs(name, partyId).once().resolves(candidate)

        const result = await appAgent.post('/v0/candidates').send(payload)

        expect(result).to.be.json
        expect(result).to.have.status(201)
        expect(result).to.have.header('Location', `/v0/candidates/${result.body.uuid}`)
        expect(result.body).to.deep.equal(candidate)
        mockCandidate.verify()
    })

    it('should fail when no name specified', async () => {
        const name = 'Platypus'
        const partyId = 'PARTY_UUID'
        const payload = {}

        mockCandidate.expects('add').withExactArgs(name, partyId).never()

        const result = await appAgent.post('/v0/candidates').send(payload)

        expect(result).to.be.json
        expect(result).to.have.status(400)
        mockCandidate.verify()
    })

    it('should fail when no party uuid specified', async () => {
        const name = 'Platypus'
        const partyId = 'PARTY_UUID'
        const payload = {name}

        mockCandidate.expects('add').withExactArgs(name, partyId).never()

        const result = await appAgent.post('/v0/candidates').send(payload)

        expect(result).to.be.json
        expect(result).to.have.status(400)
        mockCandidate.verify()
    })
})