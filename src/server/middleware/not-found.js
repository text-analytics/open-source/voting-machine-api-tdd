import { NotFound } from 'http-errors'

/**
 * @param {import('koa').Context} ctx
 */
async function notFound(ctx) {
    throw new NotFound(`Not Found: endpoint ${ctx.request.url}`)
}

export default () => {
    return notFound
}