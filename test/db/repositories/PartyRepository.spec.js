import sinon from 'sinon'
import { expect } from 'chai'

import Pool from '../../../src/db/Pool'
import Party from '../../../src/db/repositories/PartyRepository'

describe('Party Repository', async () => {

    let mockPool
    
    beforeEach(async () => {
        mockPool = sinon.mock(Pool)
    })

    afterEach(async () => {
        mockPool.restore()
    })

    it('should list all parties', async () => {
        const sql = `SELECT * FROM ${Party.tableName}`
        const rows = [{uuid: 'MONKEY_UUID', name: 'MONKEY'}, {uuid: 'PLATYPUS_UUID', name: 'PLATYPUS'}]

        mockPool.expects('query').withExactArgs(sql).once().returns({ rows })

        const result = await Party.list()

        expect(result).to.deep.equal(rows)
        mockPool.verify()
    })

    it('should add a party', async () => {
        const name = 'Platypus'
        const sql = `INSERT INTO ${Party.tableName}(name) VALUES ($1) RETURNING *`
        const party = { uuid: 'PLATYPUS_UUID', name }

        mockPool.expects('query').withExactArgs(sql, [name]).once().returns({rows: [party]})

        const result = await Party.add(name)

        expect(result).to.deep.equal(party)
        mockPool.verify()
    })
})