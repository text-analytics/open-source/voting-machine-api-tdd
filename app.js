import databaseMigrations from './src/db/migrate'
import { startServer } from './src/server'

const PORT = 2525

console.log('######## Voting Machine API ########')

await databaseMigrations()

startServer(PORT, () => {
    console.log('======= Service listening =======')
})