import Pool from '../Pool'
import Repository from './Repository'

class CandidateRepository extends Repository {

    constructor() {
        super('candidates')
    }

    async list() {
        const sql = `SELECT * FROM ${this.tableName}`
        const { rows } = await Pool.query(sql)
        return rows
    }

    /**
     * @param {string} name Candidate Name
     * @param {string} partyId Party uuid
     */
    async add(name, partyId) {
        const sql = `INSERT INTO ${this.tableName}(name,party_uuid) VALUES ($1,$2) RETURNING *`
        const { rows: [newCandidate] } = await Pool.query(sql, [name, partyId])
        return newCandidate
    }
}

/** @type {CandidateRepository} Singleton CandidateRepository Instance */
export default new CandidateRepository()