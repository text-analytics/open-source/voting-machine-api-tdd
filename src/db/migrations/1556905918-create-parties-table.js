/**
 * @param {import('node-pg-migrate').MigrationBuilder} pgm 
 */
export function up(pgm) {
    pgm.createTable('parties', {
        uuid: {
            type: 'uuid',
            primaryKey: true,
            default: pgm.func('uuid_generate_v4()')
        },
        name: {
            type: 'text',
            notNull: true,
            unique: true,
            check: "name <> ''"
        }
    })
}

export const down = false