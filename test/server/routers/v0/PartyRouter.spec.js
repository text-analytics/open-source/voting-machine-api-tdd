import sinon from 'sinon'
import { expect } from 'chai'

import appAgent from '../../../app-agent'

import Party from '../../../../src/db/repositories/PartyRepository'

describe('Party Router', async () => {

    let mockParty
    
    beforeEach(async () => {
        mockParty = sinon.mock(Party)
    })

    afterEach(async () => {
        mockParty.restore()
    })


    it('should list parties', async () => {
        const parties = [{uuid: 'MONKEY_UUID', name: 'MONKEY'}, {uuid: 'PLATYPUS_UUID', name: 'PLATYPUS'}]

        mockParty.expects('list').once().resolves(parties)

        const result = await appAgent.get('/v0/parties')

        expect(result).to.be.json
        expect(result).to.have.status(200)
        expect(result.body).to.deep.equal(parties)
        mockParty.verify()
    })

    it('should add a new party', async () => {
        const name = 'Platypus'
        const payload = { name }
        const party = {uuid: 'PLATYPUS_UUID', name }

        mockParty.expects('add').withExactArgs(name).once().resolves(party)

        const result = await appAgent.post('/v0/parties').send(payload)

        expect(result).to.be.json
        expect(result).to.have.status(201)
        expect(result).to.have.header('Location', `/v0/parties/${result.body.uuid}`)
        expect(result.body).to.deep.equal(party)
        mockParty.verify()
    })

    it('should fail when no party name specified', async () => {
        const name = 'Platypus'
        const payload = {}

        mockParty.expects('add').withExactArgs(name).never()

        const result = await appAgent.post('/v0/parties').send(payload)

        expect(result).to.be.json
        expect(result).to.have.status(400)
        mockParty.verify()
    })
})