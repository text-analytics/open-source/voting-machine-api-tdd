export default class Repository {
    /**
     * @param {string} tableName The name of the table in the database
     */
    constructor(tableName) {
        this._tableName = tableName
    }

    /**
     * @returns {string} The name of the table being accessed by this Repository
     */
    get tableName() {
        return this._tableName
    }
}