#### Base ####
FROM node:10-alpine as base

WORKDIR /usr/src/app
COPY . /usr/src/app/

RUN apk update &&\
    apk upgrade --no-cache &&\
    apk add --no-cache git &&\
    yarn --prod

# app port
EXPOSE 2525


#### Production ####
FROM base as production

ENV NODE_ENV=production

CMD ["node", "index.js"]


#### Development ####
FROM base

ENV NODE_ENV=development

RUN yarn

# debug port
EXPOSE 2526

CMD ["yarn", "start"]