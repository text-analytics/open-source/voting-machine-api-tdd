import Router from 'koa-router'
import { BadRequest } from 'http-errors'

import Party from '../../../db/repositories/PartyRepository'

export default class RootRouter extends Router {

    constructor() {
        super({ prefix: '/parties'})

        this.get('/', this.list.bind(this))
        this.post('/', this.add.bind(this))
    }

    /**
     * @swagger
     * definitions:
     * 
     *   NewParty:
     *     type: object
     *     required:
     *       - name
     *     properties:
     *       name:
     *         type: string
     *   
     *   Party:
     *     allOf:
     *       - $ref: '#/definitions/NewParty'
     *       - type: object
     *       - required:
     *         - uuid
     *         properties:
     *           uuid:
     *             type: string
     *             format: uuid
     *             example: 00000000-0000-0000-0000-000000000000
     */

    /**
     * @swagger
     * /parties:
     *   get:
     *     tags:
     *       - Parties
     *     summary: List Parties
     *     produces:
     *      - application/json
     *     responses:
     *       200:
     *         description: Party List
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/Party'
     * @private
     * @param {import('koa-router').RouterContext} ctx context
     */
    async list(ctx) {
        ctx.body = await Party.list()
    }

    /**
     * @swagger
     * /parties:
     *   post:
     *     tags:
     *       - Parties
     *     summary: Create Party
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: party
     *         description: new party object
     *         in:  body
     *         required: true
     *         type: string
     *         schema:
     *           $ref: '#/definitions/NewParty'
     *     responses:
     *       201:
     *         description: party
     *         schema:
     *           $ref: '#/definitions/Party'
     * @private
     * @param {import('koa-router').RouterContext} ctx context
     */
    async add(ctx) {
        const { name } = ctx.request.body
        if (!name) throw new BadRequest("missing required body parameter 'name'")

        const party = await Party.add(name)

        ctx.status = 201
        ctx.set('Location', `/v0/parties/${party.uuid}`)
        ctx.body = party
    }
}
