import Koa from 'koa'
import cors from '@koa/cors'
import morgan from 'koa-morgan'
import bodyParser from 'koa-bodyparser'

import errorHandler from './middleware/error-handler'
import produceJson from './middleware/produce-json'
import notFound from './middleware/not-found'
import V0Router from './routers/V0Router'
import RootRouter from './routers/RootRouter'

const app = new Koa()

app.use(cors({ origin: '*' }))
app.use(produceJson())
app.use(errorHandler())
app.use(bodyParser())

if (process.env.NODE_ENV !== 'test') {
    app.use(morgan('dev'))
}

/**
 * @param {number} port 
 * @param {function} cb 
 * @returns {import('http').Server}
 */
export const startServer = (port = 0, cb = () => {}) => {

    app.use(new V0Router().routes())
    app.use(new RootRouter().routes())

    app.use(notFound())

    const server = app.listen(port, cb)
    return server
}
