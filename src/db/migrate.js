import { Client } from 'pg'
import migrate from 'node-pg-migrate'

import { config as poolConfig } from './Pool'

export default async () => {
    try {
        await createDatabase(poolConfig.database)
        await migrate({
            databaseUrl: poolConfig,
            migrationsTable: 'migrations',
            dir: 'src/db/migrations',
            direction: 'up'
        })
    } catch(err) {
        console.error('error running migrations', err.stack)
    }
}

/**
 * @private
 * @param {string} name database name
 */
const createDatabase = async (name) => {
    const databaseConfig = Object.assign({}, poolConfig, { database: 'postgres' })
    const client = new Client(databaseConfig)

    await client.connect()

    const { rows } = await client.query(`SELECT datname FROM pg_catalog.pg_database WHERE datname = '${name}'`)
    if(rows.length === 0) {
        console.log(`creating ${name} database...`)
        await client.query(`CREATE DATABASE ${name}`)
    }

    await client.end()
}