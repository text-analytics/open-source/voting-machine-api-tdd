import sinon from 'sinon'
import { expect } from 'chai'

import Pool from '../../../src/db/Pool'
import Candidate from '../../../src/db/repositories/CandidateRepository'

describe('Candidate Repository', async () => {

    let mockPool
    
    beforeEach(async () => {
        mockPool = sinon.mock(Pool)
    })

    afterEach(async () => {
        mockPool.restore()
    })

    it('should list all candidates', async () => {
        const sql = `SELECT * FROM ${Candidate.tableName}`
        const rows = [{uuid: 'MONKEY_UUID', name: 'MONKEY', party_uuid: 'PARTY_1_UUID'}, {uuid: 'PLATYPUS_UUID', name: 'PLATYPUS', party_uuid: 'PARTY_2_UUID'}]

        mockPool.expects('query').withExactArgs(sql).once().returns({ rows })

        const result = await Candidate.list()

        expect(result).to.deep.equal(rows)
        mockPool.verify()
    })

    it('should add a candidate', async () => {
        const name = 'Platypus'
        const partyId = 'PARTY_UUID'
        const sql = `INSERT INTO ${Candidate.tableName}(name,party_uuid) VALUES ($1,$2) RETURNING *`
        const candidate = { uuid: 'PLATYPUS_UUID', name, party_uuid: partyId }

        mockPool.expects('query').withExactArgs(sql, [name, partyId]).once().returns({rows: [candidate]})

        const result = await Candidate.add(name, partyId)

        expect(result).to.deep.equal(candidate)
        mockPool.verify()
    })
})