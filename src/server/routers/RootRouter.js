import Router from 'koa-router'

export default class RootRouter extends Router {

    constructor() {
        super()

        this.get('/', this.swaggerRedirect.bind(this))
        this.get('/ping', this.ping.bind(this))
    }

    /**
     * @private
     * @param {import('koa-router').RouterContext} ctx context
     */
    async swaggerRedirect(ctx) {
        ctx.redirect('/v0/swagger')
    }

    /**
     * @private
     * @param {import('koa-router').RouterContext} ctx context
     */
    async ping(ctx) {
        ctx.body = 'pong'
    }
}
