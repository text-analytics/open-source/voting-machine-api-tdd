/**
 * @param {import('node-pg-migrate').MigrationBuilder} pgm 
 */
export function up(pgm) {
    const tableName = 'candidates'

    pgm.createTable(tableName, {
        uuid: {
            type: 'uuid',
            primaryKey: true,
            default: pgm.func('uuid_generate_v4()')
        },
        name: {
            type: 'text',
            notNull: true,
            unique: true,
            check: "name <> ''"
        },
        party_uuid: {
            type: 'uuid',
            notNull: true,
            references: 'parties',
            onDelete: 'CASCADE'
        }
    })

    pgm.createIndex(tableName, 'party_uuid')
}

export const down = false