/**
 * @param {import('koa').Context} ctx
 * @param {Promise} next
 */
async function produceJson(ctx, next) {
    await next()
    if(!ctx.url.endsWith('/swagger')) {
        ctx.type = 'json'
        if (ctx.body) {
            ctx.body = JSON.stringify(ctx.body)
        } else {
            ctx.status = 204
        }
    }
}

export default () => {
    return produceJson
}